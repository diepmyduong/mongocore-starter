const { firebaseService } = require('../dist/server/v1/services/firebase');
const { chatfuelService } = require('../dist/server/v1/services/crud/chatfuel');
const { userService } = require('../dist/server/v1/services/crud/user');
const { formService } = require('../dist/server/v1/services/crud/form');
const _ = require('lodash');

firebaseService.app.database().ref('member').once('value', (async snap => {
    const members = _.values(snap.val());
    let forms = [];
    for (let m of members) {
        if (m.forms) {
            _.values(m.forms).forEach(f => {
                forms.push(_.merge({}, f, { psid: m.psid }));
            })
        }
    }

    console.log('not uniq', forms.length);
    const users = await userService.model.findAll({});
    const chatfuels = await chatfuelService.model.findAll({});
    console.log('user', users.length);
    console.log('chatfuels', chatfuels.length);
    for (let f of forms) {
        user = users.find(u => u.psid == f.psid);
        if (!user) { console.log('user not exist', f.psid); continue; }
        bot = chatfuels.find(b => {
            return b.userId == user.id && b.botId == f.form.cf_botid && b.token == f.form.cf_token;
        })
        if (!bot) { console.log('bot not exist', f.form.cf_botid); continue; }
        const filter = {
            userId: user.id,
            code: f.formId
        }
        const data = {
            userId: user.id,
            chatfuelId: bot.id,
            code: f.formId,
            name: f.form.formName,
            setting: {
                btnText: f.form.btnText,
                schema: f.schema
            }
        }
        
        await formService.model.findOrCreate({
            where: filter, defaults: data
        })
        console.log('create form', f.form.formName, f.form.cf_botid, user.email);
    }
}))