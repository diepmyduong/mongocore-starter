const { firebaseService } = require('../dist/server/v1/services/firebase');
const { userService } = require('../dist/server/v1/services/crud/user');
const _ = require('lodash');

userService.model.findAll({
    where: { 
        psid: { $ne: null },
        uid: { $eq: null }
    }
}).then(async users => {
    let successCount = 0;
    let failCount = 0;
    for(let u of users) {
        const fbUser = await firebaseService.createUser({ email: u.email, password: u.code })
            .catch(err => {
                console.log('cannot create user with account ', u.email);
                failCount++;
            })
        if (fbUser) {
            u.set('uid', fbUser.uid);
            await u.save();
            successCount++;
        }
    }
    console.log('success', successCount);
    console.log('failed', failCount);
})