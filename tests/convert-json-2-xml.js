const { utilService } = require('../dist/server/v1/services');
const fs = require('fs');
const json = JSON.parse(fs.readFileSync('./tests/xml2json.json', 'utf8'));

const xml = utilService.jsonToXml(json);

fs.writeFileSync('./tests/js2xml.xml', xml);