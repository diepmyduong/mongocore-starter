const { utilService } = require('../dist/server/v1/services');

const xmlContent = `
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type='text/xsl' href='../template/01GTKT00010312671405?action=true' ?>
<inv:invoice xmlns:inv="http://laphoadon.gdt.gov.vn/2014/09/invoicexml/v1" xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
   <inv:invoiceData id="data">
      <inv:sellerAppRecordId />
      <inv:invoiceAppRecordId>@invoiceAppRecordId</inv:invoiceAppRecordId>
      <inv:invoiceType>@invoiceType</inv:invoiceType>
      <inv:invoiceSigned>False</inv:invoiceSigned>
      <inv:vatPercentageBill>0</inv:vatPercentageBill>
      <inv:referentNo />
      <inv:printFlag>False</inv:printFlag>
      <inv:printSample>False</inv:printSample>
      <inv:templateCode>01GTKT0/001</inv:templateCode>
      <inv:adjustmentType />
      <inv:originalInvoice />
      <inv:invoiceSeries>AA/19E</inv:invoiceSeries>
      <inv:invoiceNumber>0000051</inv:invoiceNumber>
      <inv:invoiceName>HÓA ĐƠN GIÁ TRỊ GIA TĂNG</inv:invoiceName>
      <inv:invoiceIssuedDate>19/03/2019</inv:invoiceIssuedDate>
      <inv:signedDate>Tue Mar 19 2019 07:00:00 GMT+0700 (+07)</inv:signedDate>
      <inv:submittedDate>@submittedDate</inv:submittedDate>
      <inv:contractNumber />
      <inv:currencyCode>VND</inv:currencyCode>
      <inv:ExchangeRate>1</inv:ExchangeRate>
      <inv:invoiceNote />
      <inv:adjustmentType />
      <inv:paymentMethodName>Tiền mặt/Chuyển khoản</inv:paymentMethodName>
      <inv:additionalReferenceDesc />
      <inv:payments>
         <inv:payment>
            <inv:paymentMethodNameExt>Tiền mặt/Chuyển khoản</inv:paymentMethodNameExt>
         </inv:payment>
      </inv:payments>
      <inv:delivery />
      <inv:sellerLegalName>CÔNG TY CỔ PHẦN TOTAL</inv:sellerLegalName>
      <inv:sellerTaxCode>0312671405</inv:sellerTaxCode>
      <inv:sellerAddressLine>123456789012345</inv:sellerAddressLine>
      <inv:sellerPhoneNumber>0312303803</inv:sellerPhoneNumber>
      <inv:sellerFaxNumber>0312303803</inv:sellerFaxNumber>
      <inv:sellerEmail>info@cnx.vn</inv:sellerEmail>
      <inv:sellerWebsite>www.cnx.vn</inv:sellerWebsite>
      <inv:sellerBankName>Ngân Hàng TMCP Á Châu</inv:sellerBankName>
      <inv:sellerBankAccount>09993-00939-39</inv:sellerBankAccount>
      <inv:sellerSignedPersonName>@sellerSignedPersonName</inv:sellerSignedPersonName>
      <inv:sellerContactPersonName />
      <inv:sellerSubmittedPersonName />
      <inv:buyerDisplayName />
      <inv:buyerLegalName>nhien</inv:buyerLegalName>
      <inv:buyerTaxCode>987546312</inv:buyerTaxCode>
      <inv:buyerAddressLine>191 ba trieu</inv:buyerAddressLine>
      <inv:buyerPhoneNumber>@buyerPhoneNumber</inv:buyerPhoneNumber>
      <inv:buyerFaxNumber />
      <inv:buyerEmail>nhienvth@bidv.com.vn</inv:buyerEmail>
      <inv:buyerBankName />
      <inv:buyerBankAccount />
      <inv:items>
         <inv:item>
            <inv:lineNumber>1</inv:lineNumber>
            <inv:itemCode />
            <inv:itemName>Trà hoa cúc</inv:itemName>
            <inv:unitName />
            <inv:unitPrice>435000</inv:unitPrice>
            <inv:quantity>1</inv:quantity>
            <inv:currency>VND</inv:currency>
            <inv:itemTotalAmountWithoutVat>435000</inv:itemTotalAmountWithoutVat>
            <inv:itemDiscount>0</inv:itemDiscount>
            <inv:vatPercentage>0</inv:vatPercentage>
            <inv:vatAmount>0</inv:vatAmount>
            <inv:itemAmount>435000</inv:itemAmount>
         </inv:item>
         <inv:item>
            <inv:lineNumber>2</inv:lineNumber>
            <inv:itemCode />
            <inv:itemName>Hồng trà hữu cơ - Đài Loan</inv:itemName>
            <inv:unitName />
            <inv:unitPrice>530000</inv:unitPrice>
            <inv:quantity>1</inv:quantity>
            <inv:currency>VND</inv:currency>
            <inv:itemTotalAmountWithoutVat>530000</inv:itemTotalAmountWithoutVat>
            <inv:itemDiscount>0</inv:itemDiscount>
            <inv:vatPercentage>0</inv:vatPercentage>
            <inv:vatAmount>0</inv:vatAmount>
            <inv:itemAmount>530000</inv:itemAmount>
         </inv:item>
      </inv:items>
      <inv:invoiceTaxBreakdowns>
         <inv:invoiceTaxBreakdown>
            <inv:vatPercentage>0</inv:vatPercentage>
            <inv:vatTaxableAmount>965000</inv:vatTaxableAmount>
            <inv:vatTaxAmount>@vatTaxAmount</inv:vatTaxAmount>
         </inv:invoiceTaxBreakdown>
      </inv:invoiceTaxBreakdowns>
      <inv:totalAmountWithoutVAT>965000</inv:totalAmountWithoutVAT>
      <inv:totalVATAmount>0</inv:totalVATAmount>
      <inv:totalAmountWithVAT>965000</inv:totalAmountWithVAT>
      <inv:totalAmountWithVATInWords>Chín  trăm  sáu  mươi lăm  nghìn đồng.</inv:totalAmountWithVATInWords>
      <inv:discountAmount>0</inv:discountAmount>
      <inv:serviceChargePercent>0</inv:serviceChargePercent>
      <inv:totalServiceCharge>0</inv:totalServiceCharge>
      <inv:userDefines />
      <inv:systemCode />
      <inv:qrCodeData />
   </inv:invoiceData>
   <inv:controlData />
   <Signature xmlns="http://www.w3.org/2000/09/xmldsig#" Id="seller">
      <SignedInfo>
         <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />
         <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />
         <Reference URI="">
            <Transforms>
               <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />
               <Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />
            </Transforms>
            <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />
            <DigestValue>gNng6GQ03AfYBnr0Ohdux88OiE8=</DigestValue>
         </Reference>
      </SignedInfo>
      <SignatureValue>PPt0lNvJqjDC+V53fisvapVO8JwxUP/pGbFFBIWrIfiGL+nIFU7TajhYw4S143l2MZ+dcuXPp0E0azQn7tI39IdOV24IACmXcGFVx4VhAzcbaF19e1pfx2/JjNyMmf7nqtCJkbJomcM7s8sJ8IyEyqWbfFvKrHil7LR8Pav6oBWpWpQWWecoLswriTVgmCDC+liWb3me+83zKkg8jpXeXNk5tiphZ4RwdyaDdfHhLtzJWcHdGlDpuq4fI9P8x7Mva7cDtD5Cu6OMQdsw1rmOqtywuxiN+FUIyXJAmC2OHELN5I/tKnQk5TmU++Pqf+Q2opL66KceHEYfFhdKYZiuTw==</SignatureValue>
      <KeyInfo>
         <KeyName>Public key of certificate</KeyName>
         <KeyValue>
            <RSAKeyValue>
               <Modulus>x/TLlgswzsTxLJ47GgyJcVpS4Bi+IZIIFRujcrx7NtSIYVEIybtFAjklgd1FQ7SOoELGLXu3HO5aBLrtEGY3OHLhZLF7bGKscL+jLiH30aRHGy/e0Ida5PTgGTL8bscxFJ9JwBbSJQqD6YrZucJLiCQdgeCoXotuI37QetY6W53fY+6yB8uWL0d4tM+wwXiBeP31/kEwAMJujVXzMnCs3YES2C6xGn74I9tukjmTqXemCjjtvHFd7frtX4QzP2lC/a5iyrcJoBKBk34zmRB3kbuNZGBLKof60hMySxZPwgdNROUrEfCD4MhF6nlLSWXeY/KyS7m31FOJ51/Sb6Zxjw==</Modulus>
               <Exponent>AQAB</Exponent>
            </RSAKeyValue>
         </KeyValue>
         <X509Data>
            <X509Certificate>MIIEDzCCAvegAwIBAgIQVARW0UzU9dCIhak0HbhB2TANBgkqhkiG9w0BAQsFADBXMRIwEAYDVQQDDAlTbWFydFNpZ24xEjAQBgNVBAsMCVNtYXJ0U2lnbjESMBAGA1UECgwJU21hcnRTaWduMQwwCgYDVQQIDANIQ00xCzAJBgNVBAYTAlZOMB4XDTE3MDQxMzA5MTc0NloXDTE5MDQxMzA5MTI1OFowgaIxPDA6BgNVBAMMM0PDlE5HIFRZIFROSEggTeG7mFQgVEjDgE5IIFZJw4pOIFRIxq/GoE5HIE3huqBJIEsuTjEXMBUGA1UECwwOTVNUOjAzMTI2NzE0MDUxPDA6BgNVBAoMM0PDlE5HIFRZIFROSEggTeG7mFQgVEjDgE5IIFZJw4pOIFRIxq/GoE5HIE3huqBJIEsuTjELMAkGA1UEBhMCVk4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDH9MuWCzDOxPEsnjsaDIlxWlLgGL4hkggVG6NyvHs21IhhUQjJu0UCOSWB3UVDtI6gQsYte7cc7loEuu0QZjc4cuFksXtsYqxwv6MuIffRpEcbL97Qh1rk9OAZMvxuxzEUn0nAFtIlCoPpitm5wkuIJB2B4Khei24jftB61jpbnd9j7rIHy5YvR3i0z7DBeIF4/fX+QTAAwm6NVfMycKzdgRLYLrEafvgj226SOZOpd6YKOO28cV3t+u1fhDM/aUL9rmLKtwmgEoGTfjOZEHeRu41kYEsqh/rSEzJLFk/CB01E5SsR8IPgyEXqeUtJZd5j8rJLubfUU4nnX9JvpnGPAgMBAAGjgYowgYcwHQYDVR0OBBYEFGrejV9mPa1+HYsOJ445vFS6887RMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUb1bE+AFluzMau+dFD1JDmR+XsQgwDgYDVR0PAQH/BAQDAgPoMCcGA1UdJQQgMB4GCCsGAQUFBwMBBggrBgEFBQcDAgYIKwYBBQUHAwQwDQYJKoZIhvcNAQELBQADggEBADPuLVqoTeYUODnpuyIvtdDfXInhRQrL13OTaK8b5hkZObtPojggRytFlAoUGmVpBzj6TSRE/Wmsz0+iuFcYajowE2fO6CMI3MpxRLGT4GDIHaV7pwo2IEMTgCH+Sh3dkz4FbZljTR21gc+3xZp2YKKmFNMs8pFJKPC2RFBVnVgsu4m30VCVw4zvhMDChVuBIujQemK30yA16OzzOe+jsG4cJ3tfYfBLlb2tdRFP8kmsp5gFl2TtkWEOzlDydyIuVUMqse37xAFDAH1YmKF4qfyZc6R0D8VzCMDOTTPmISbOqe8NWEeeAcg4Pvik/XTAI4uwdFNoWog7BFG734dgM+0=</X509Certificate>
         </X509Data>
      </KeyInfo>
   </Signature>
</inv:invoice>
`

const fs = require('fs');

utilService.xmlToJson(xmlContent).then(result => {
    fs.writeFileSync('./tests/xml2json.json', JSON.stringify(result, null, 2));
}).catch(err => {
    console.log('ERROR', err);
})