const { firebaseService } = require('../dist/server/v1/services/firebase');
const { userService } = require('../dist/server/v1/services/crud/user');
const _ = require('lodash');

firebaseService.app.database().ref('member').once('value', (async snap => {
    const members = _.values(snap.val());
    let newMembers = [];
    const psids = members.map(m => m.psid);
    const { count, rows: existUsers} = await userService.getList({ limit: 0, filter: { psid: { $in: psids }}});
    const ids = existUsers.map(u => u.psid).join(",");
    newMembers = members.filter(m => !ids.includes(m.psid));
    userService.model.bulkCreate(newMembers.map(m => {
        return {
            name: m.hovaten,
            class: m.khoadangky,
            code: m.maSuDung,
            point: 14,
            email: m.email,
            phone: m.dienthoai,
            psid: m.psid
        }
    })).then(res => {
        console.log('clone success', newMembers.length);
    })
}))