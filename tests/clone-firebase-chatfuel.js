const { firebaseService } = require('../dist/server/v1/services/firebase');
const { chatfuelService } = require('../dist/server/v1/services/crud/chatfuel');
const { userService } = require('../dist/server/v1/services/crud/user');
const _ = require('lodash');

firebaseService.app.database().ref('member').once('value', (async snap => {
    const members = _.values(snap.val());
    let bots = [];
    for (let m of members) {
        if (m.forms) {
            _.values(m.forms).forEach(f => {
                bots.push({
                    psid: m.psid,
                    cf_botid: f.form.cf_botid,
                    cf_token: f.form.cf_token,
                    name: f.formId
                })
            })
        }
    }

    console.log('not uniq', bots.length);
    bots = _.uniqWith(bots, (a, b) => {
        return a.psid == b.psid && a.cf_botid == b.cf_botid && a.cf_token == b.cf_token;
    });
    console.log('bots', bots.length);
    const users = await userService.model.findAll();
    for (let b of bots) {
        user = users.find(u => u.psid == b.psid);
        await chatfuelService.model.findOrCreate({
            where: {
                userId: user.id,
                botId: b.cf_botid
            }, defaults: {
                userId: user.id,
                botId: b.cf_botid,
                token: b.cf_token,
                name: b.name
            }
        })
        console.log('create bot', b.name, b.cf_botid, user.email);
    }
}))