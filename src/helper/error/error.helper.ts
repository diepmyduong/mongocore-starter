import { BaseError, ErrorHelper as BaseErrorHelper } from './baseError';

export class ErrorHelper extends BaseErrorHelper {
  static userNotExist() {
    return new BaseError(403, '-103', 'Người dùng khồng tồn tại');
  }
  static userExisted() {
    return new BaseError(403, '-104', 'Người dùng đã tồn tại');
  }
  static userRoleNotSupported() {
    return new BaseError(401, '-105', 'Người dùng không được cấp quyền');
  }
}
