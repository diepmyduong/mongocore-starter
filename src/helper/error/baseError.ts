import express from 'express';

export interface IErrorInfo {
  status: number;
  code: string;
  message: string;
  data?: any;
}

export class BaseError extends Error {
  constructor(status: number, code: string, message: string, data?: any) {
    super(message);
    this.info = { status, code, message, data };
  }
  info: IErrorInfo;
}
export class ErrorHelper {
  static handleError(
    func: (req: express.Request, rep: express.Response) => Promise<any>
  ) {
    return (req: express.Request, res: express.Response) =>
      func
        .bind(this)(req, res)
        .catch((error: any) => {
          if (!error.info) {
            console.log('UNKNOW ERROR', error);
            const err = ErrorHelper.somethingWentWrong();
            res.status(err.info.status).json(err.info);
          } else {
            res.status(error.info.status).json(error.info);
          }
        });
  }
  static logError(prefix: string, logOption = true) {
    return (error: any) => {
      console.log(
        prefix,
        error.message || error,
        logOption ? error.options : ''
      );
      // try {
      //     sentryService.raven.captureException(error);
      // } catch (err) {
      //     console.log('cannot send to sentry');
      // }
    };
  }
  // Unknow
  static somethingWentWrong(message?: string) {
    return new BaseError(500, '500', message || 'Có lỗi xảy ra');
  }
  // Auth
  static unauthorized() {
    return new BaseError(401, '401', 'Chưa xác thực');
  }
  static badToken() {
    return new BaseError(401, '-1', 'Không có quyền truy cập');
  }
  static tokenExpired() {
    return new BaseError(401, '-2', 'Mã truy cập đã hết hạn');
  }
  static permissionDeny() {
    return new BaseError(405, '-3', 'Không đủ quyền để truy cập');
  }
  // Request
  static requestDataInvalid(message: string) {
    return new BaseError(403, '-4', 'Dữ liệu gửi lên không hợp lệ');
  }
  // External Request
  static externalRequestFailed(message: string) {
    return new BaseError(500, '-5', 'Có lỗi xảy ra');
  }
  // Mongo
  static mgRecoredNotFound() {
    return new BaseError(404, '-7', 'Không tìm thấy dữ liệu yêu cầu');
  }
  static mgQueryFailed(message: string) {
    return new BaseError(403, '-8', message || 'Truy vấn không thành công');
  }
}
