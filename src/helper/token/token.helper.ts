import jwt from 'jwt-simple';
import moment from 'moment';

import { config } from '../../config';
import { ErrorHelper } from '../error/error.helper';

export interface IGenerateTokenOption {
  exp?: moment.Moment;
  secret?: string;
}
export interface IDecodeTokenOption {
  secret?: string;
}
export class TokenHelper {
  static generateToken(
    payload: any,
    role: string,
    option: IGenerateTokenOption = {}
  ): string {
    const secret = option.secret || config.secret;
    return jwt.encode(
      {
        payload: payload,
        role: role,
        exp: option.exp || moment().add(1, 'days')
      },
      secret
    );
  }
  static decodeToken(token: string, option: IDecodeTokenOption = {}) {
    let result = undefined;
    try {
      const secret = option.secret || config.secret;
      result = jwt.decode(token, secret);
    } catch (err) {
      throw ErrorHelper.badToken();
    }
    if (result) {
      if (new Date(result.exp).getTime() <= Date.now()) {
        throw ErrorHelper.tokenExpired();
      }
      return result;
    } else {
      throw ErrorHelper.badToken();
    }
  }
}
