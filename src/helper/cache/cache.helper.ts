import _ from 'lodash';
import NodeCache from 'node-cache';

export type Bucket = {
  bucketName: string;
  cache: NodeCache;
};
const buckets: Bucket[] = [];

export class CacheHelper {
  static getBucket(bucketName: string, option: any = {}) {
    let bucket = buckets.find(b => b.bucketName == bucketName);
    if (!bucket) {
      const cache = new NodeCache(
        _.merge({ stdTTL: 300, checkperiod: 120, useClones: false }, option)
      );
      bucket = { bucketName, cache };
      buckets.push(bucket);
    }
    return bucket;
  }

  static set(key: string, value: any, bucketName: string = 'default') {
    const bucket = CacheHelper.getBucket(bucketName);
    bucket.cache.set(key, value);
  }

  static get<T>(key: string, bucketName: string = 'default') {
    const bucket = CacheHelper.getBucket(bucketName);
    return bucket.cache.get(key) as T;
  }

  static clear(bucketName: string) {
    const bucket = CacheHelper.getBucket(bucketName);
    bucket.cache.flushAll();
  }
}
