import admin from 'firebase-admin';
import _ from 'lodash';

import { config } from '../../config';
import { ErrorHelper } from '../error/error.helper';

export const FirebaseApp = admin.initializeApp(
  {
    credential: admin.credential.cert(
      config.firebaseCredential as admin.ServiceAccount
    ),
    databaseURL: `https://${config.firebaseCredential.project_id}.firebaseio.com`,
    storageBucket: `${config.firebaseCredential.project_id}.appspot.com`
  },
  'v1'
);

let realtimeConfig = {};

if (config.enableRealtimeConfig) {
  FirebaseApp.database()
    .ref('system')
    .on('value', snap => {
      if (snap) {
        realtimeConfig = snap.val();
        console.log('update system config', realtimeConfig);
      }
    });
}
FirebaseApp.database().ref('system');

export class FirebaseHelper {
  static getConfig(path?: string) {
    if (path) {
      return _.get(realtimeConfig, path);
    } else {
      return realtimeConfig;
    }
  }
  static async verifyIdToken(token: string) {
    try {
      return await FirebaseApp.auth().verifyIdToken(token);
    } catch (err) {
      console.log('err', err);
      throw ErrorHelper.badToken();
    }
  }
  static async createUser(email: string, password: string) {
    return await FirebaseApp.auth().createUser({ email, password });
  }
  static async removeUser(uid: string) {
    return await FirebaseApp.auth().deleteUser(uid);
  }
  static async getUserByEmail(email: string) {
    return await FirebaseApp.auth().getUserByEmail(email);
  }
  static async getUserByPhone(phone: string) {
    return await FirebaseApp.auth().getUserByPhoneNumber(phone);
  }
  static async getUserById(uid: string) {
    return await FirebaseApp.auth().getUser(uid);
  }
  static async uploadFile(path: string, bucket?: string) {
    return await FirebaseApp.storage()
      .bucket(bucket)
      .upload(path);
  }
  static async getFile(filename: string, bucket?: string) {
    return await FirebaseApp.storage()
      .bucket(bucket)
      .file(filename)
      .download();
  }
  static async deleteFile(filename: string, bucket?: string) {
    return await FirebaseApp.storage()
      .bucket(bucket)
      .file(filename)
      .delete();
  }
}
