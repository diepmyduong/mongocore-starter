import { ErrorHelper } from '../helper/error/baseError';
import { TokenHelper } from '../helper/token/token.helper';
import { userService } from '../v1/user/user.service';
import { BaseMiddleware } from '../core/baseMiddleware';
import { NextFunction, Request, Response } from '../core/interfaces';

export class AuthInfoMiddleware extends BaseMiddleware {
  async use(
    req: Request,
    res: Response,
    next: NextFunction,
    providers: string[] = []
  ) {
    if (providers.indexOf('block') != -1) {
      throw ErrorHelper.permissionDeny();
    }
    if (!req.headers['x-token'] && req.query['x-token']) {
      req.headers['x-token'] = req.query['x-token'];
    }
    if (req.headers['x-token']) {
      const [type, id, token] = req.headers['x-token']!.toString().split('|');
      req.authInfo = { id, provider: '*' };
      switch (type) {
        case 'U':
          const decodeToken = TokenHelper.decodeToken(token);
          req.authInfo.user = new userService.model(decodeToken.payload);
          req.authInfo.provider = decodeToken.role;
          await this.permissionVerify(req, providers);
          break;
        case 'ANY':
          if (!providers.includes('*')) {
            throw ErrorHelper.permissionDeny();
          }
          break;
        default:
          throw ErrorHelper.badToken();
      }
      next();
      return;
    }
    throw ErrorHelper.unauthorized();
  }
  async permissionVerify(req: Request, providers: string[], option = {}) {
    for (const p of providers) {
      const regex = new RegExp('^' + p + '$');
      for (const r of req.authInfo!.provider!.split(' ')) {
        if (regex.test(r)) return true;
      }
    }
    throw ErrorHelper.permissionDeny();
  }
}

export const authMid = new AuthInfoMiddleware();
