import _ from 'lodash';

import { config } from '../config';
import { BaseMiddleware } from '../core/baseMiddleware';
import { NextFunction, Request, Response } from '../core/interfaces';

class QueryMiddleware extends BaseMiddleware {
  async use(req: Request, res: Response, next: NextFunction, option: any) {
    const filter = this._parseFilter(req, option);
    const order = this._parseOrder(req);
    const page = parseInt(req.query['page'] || 1);
    const limit = parseInt(req.query['limit'] || config.pageSize);
    const offset = parseInt(req.query['offset']) || (page - 1) * limit;
    const fields = this._parseMongoFields(req);
    const populates = this._parsePopulates(req);

    req.queryInfo = _.merge({
      filter,
      limit,
      offset,
      fields,
      populates,
      order,
      cached: req.query['cached'] == "true"
    });

    next();
  }

  /**
   * Filter param only accept <and> query. <or> will be supported later
   * Format: [[key, operator, value], [key, operator, value]]
   */
  _parseFilter(req: Request, option?: any): any {
    let filter = req.query['filter'];
    try {
      filter = JSON.parse(filter);
    } catch (ignore) {
      filter = undefined;
    }
    if (option && option.useBody) {
      filter = req.body['filter'];
    }
    return filter || {};
  }

  _parseGroup(req: Request): any {
    let group = req.query['group'];
    try {
      group = JSON.parse(group);
    } catch (err) {
      group = undefined;
    }
    return group;
  }
  /**
   * Format: [[key, order], [key, order]]
   */
  _parseOrder(req: Request): any {
    let order = req.query['order'];
    try {
      order = JSON.parse(order);
    } catch (ignore) {
      order = undefined;
    }
    return order || [];
  }
  _parseMongoFields(req: Request) {
    let fields = req.query['fields'];
    try {
      fields = JSON.parse(fields);
    } catch (ignore) {
      fields = undefined;
    }
    return fields;
  }
  _parsePopulates(req: Request) {
    let populates = req.query['populates'];
    try {
      populates = JSON.parse(populates);
    } catch (ignore) {
      populates = undefined;
    }
    return populates;
  }
}

export const queryMid = new QueryMiddleware();
