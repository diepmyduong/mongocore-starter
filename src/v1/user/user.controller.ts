import { BaseCrudController } from '../../core/adapter/mongo/crud/baseCrudController';
import { Request, Response } from '../../core/interfaces';
import { ErrorHelper } from '../../helper/error/error.helper';
import { FirebaseHelper } from '../../helper/firebase/firebase.helper';
import { UserSchema } from './user.schema';
import { UserService, userService } from './user.service';

export class UserController extends BaseCrudController<UserService> {
  constructor() {
    super(userService);
  }
  async create(req: Request, res: Response) {
    await this.validateJSON(req.body, UserSchema.createUserSchema);
    // Create firebase user
    const { email, password } = req.body;
    const firebaseUser = await FirebaseHelper.createUser(email, password).catch(
      err => {
        throw ErrorHelper.userExisted();
      }
    );
    const user = { ...req.body, uid: firebaseUser.uid };
    delete user.password;
    if (req.authInfo!.user!.role != 'admin') {
      user.role = 'client';
    }
    const result = await this.service.create(user);
    this.onSuccess(res, result);
  }
  async delete(req: Request, res: Response) {
    const { _id } = req.params;
    const result = await this.service.delete({
      filter: { id: _id }
    });
    FirebaseHelper.removeUser(result.get('uid')).catch(
      ErrorHelper.logError(`Error when remove firebase User ${result.uid}`)
    );
    this.onSuccess(res, result);
  }
}

export const userController = new UserController();
