import { IValidateSchema } from '../../core/interfaces';

export class UserSchema {
  static createUserSchema: IValidateSchema = {
    type: 'object',
    properties: {
      name: { type: 'string' },
      email: { type: 'string' },
      phone: { type: 'string' },
      address: { type: 'string' },
      password: { type: 'string' },
      role: { type: 'string' }
    },
    required: ['name', 'email', 'password'],
    additionalProperties: false
  };
}
