import { BaseCrudRouter } from '../../core/adapter/mongo/crud/baseCrudRouter';
import { userController, UserController } from './user.controller';

export default class UserRouter extends BaseCrudRouter<UserController> {
  constructor() {
    super(userController);
  }
  customRouting() {}
}
