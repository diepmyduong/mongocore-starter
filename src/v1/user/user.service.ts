import { BaseCrudService } from '../../core/adapter/mongo/crud/baseCrudService';
import { UserDoc, UserModel } from './user.model';

export class UserService extends BaseCrudService<UserDoc> {
  constructor() {
    super(UserModel);
    console.log('contructor UserService');
  }
}

export const userService = new UserService();
