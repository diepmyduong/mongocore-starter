import mongoose from 'mongoose';

// import mongooseHidden from 'mongoose-hidden';
import { MainConnection } from '../../config/connections/main';
import { BaseDocument } from '../../core/adapter/mongo/baseModel';

const Schema = mongoose.Schema;

export type UserDoc = BaseDocument & {
  uid?: string;
  name?: string;
  email?: string;
  picture?: string;
  phone?: string;
  role?: string;
};

const userSchema = new Schema(
  {
    uid: { type: String, required: true, unique: true },
    name: { type: String, required: true },
    email: { type: String },
    picture: { type: String },
    phone: { type: String },
    role: { type: String, enum: ['admin', 'user'] }
  },
  { timestamps: true }
);

// userSchema.plugin(mongooseHidden({ defaultHidden: { _id: false } }));

export let UserModel: mongoose.Model<UserDoc> = MainConnection.model(
  'User',
  userSchema
);
