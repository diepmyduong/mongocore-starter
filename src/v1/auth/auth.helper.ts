
import _ from 'lodash';

import { TokenHelper } from '../../helper/token/token.helper';
import { UserDoc } from '../user/user.model';

export class AuthHelper {
  static getUserToken(user: UserDoc) {
    const json = user.toJSON();
    const result: any = {
      token: TokenHelper.generateToken(_.omit(json), user.role!)
    };
    return result;
  }
}
