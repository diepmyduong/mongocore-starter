import { BaseController } from '../../core/baseController';
import { Request, Response } from '../../core/interfaces';
import { CacheHelper } from '../../helper/cache/cache.helper';
import { ErrorHelper } from '../../helper/error/error.helper';
import { FirebaseHelper } from '../../helper/firebase/firebase.helper';
import { userService } from '../user/user.service';
import { AuthHelper } from './auth.helper';
import { AuthSchema } from './auth.schema';

export class AuthController extends BaseController {
  constructor() {
    super();
  }
  async login(req: Request, res: Response) {
    await this.validateJSON(req.body, AuthSchema.LoginSchema);
    const { token, name, email, phone, address } = req.body;
    const [type, uid, fbtoken] = token.split('|');
    if (type == 'FB') {
      const decode = await FirebaseHelper.verifyIdToken(fbtoken);
      if (uid != decode.uid) throw ErrorHelper.badToken();
      let user = await userService.getItem({
        filter: { uid: decode.uid },
        allowNull: true,
        cached: true
      });
      if (!user) {
        let userCount = CacheHelper.get('user-count') as number;
        if (!userCount) {
          userCount = await userService.count();
          if (userCount > 0) CacheHelper.set('user-count', userCount);
        }
        user = new userService.model({
          uid: decode.uid,
          email: email || decode.email,
          name: name || decode.email,
          phone: phone,
          address: address
        });
        if (userCount == 0) {
          user.role = 'admin';
        }
        await user.save();
      }
      this.onSuccess(res, AuthHelper.getUserToken(user));
    } else {
      throw ErrorHelper.badToken();
    }
  }
}

export const authController = new AuthController();
