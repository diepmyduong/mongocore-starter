import { IValidateSchema } from '../../core/interfaces';

export class AuthSchema {
  static LoginSchema: IValidateSchema = {
    type: 'object',
    properties: {
      token: { type: 'string' },
      name: { type: 'string' },
      phone: { type: 'string' },
      email: { type: 'string' },
      address: { type: 'string' }
    },
    required: ['token', 'name', 'email', 'phone'],
    additionalProperties: false
  };
}
