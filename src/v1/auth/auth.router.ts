import { BaseRouter } from '../../core/baseRouter';
import { authController, AuthController } from './auth.controller';

export default class AuthRouter extends BaseRouter<AuthController> {
  constructor() {
    super('auth');
    this.controller = authController;
    this.customRouting();
  }
  controller: AuthController;

  customRouting() {
    this.router.post('/login', this.route(this.controller.login));
  }
}
