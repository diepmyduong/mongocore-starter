import fs from 'fs';
import path from 'path';
import express from 'express';

function regisRouter(route: express.Router, node: string, dir: string) {
  const modules = fs
    .readdirSync(dir)
    .filter(m => fs.lstatSync(path.join(dir, m)).isDirectory());
  modules.forEach(modulePath => {
    const routerPath = fs
      .readdirSync(path.join(dir, modulePath))
      .find(f => /(.*).router.js$/.test(f));
    if (routerPath) {
      const { default: Router } = require(path.join(
        dir,
        modulePath,
        routerPath
      ));
      const router = new Router();
      route.use(
        `/${router.path || /(.*).router.js$/.exec(routerPath)![1]}`,
        router.router
      );
    }
  });
  return route;
}

export default regisRouter(express.Router(), '', __dirname);
