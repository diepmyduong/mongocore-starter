export interface IValidateSchema {
  [x: string]: any;

  type?: string | string[];
  properties?: IValidateSchemaProperties;
  additionalProperties?: boolean;
  required?: string[];
  uniqueItems?: boolean;
  minItems?: number;
  items?: IValidateSchema;
}
export interface IValidateSchemaProperties {
  [x: string]: IValidateSchema;
}
