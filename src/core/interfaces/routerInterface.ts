import express from 'express';
import { ICrudOption } from './crudInterface';
import { UserDoc } from '../../v1/user/user.model';

export interface Request extends express.Request {
  [x: string]: any;

  queryInfo?: ICrudOption;
  tokenInfo?: {
    payload: any;
    roles: string;
    exp: any;
    [x: string]: any;
  };
  authInfo?: {
    id: string;
    provider: string;
    user?: UserDoc;
    [x: string]: any;
  };
}
export interface Response extends express.Response {
  [x: string]: any;
}
export interface NextFunction extends express.NextFunction {
  [x: string]: any;
}
