export interface ICrudOption {
  [x: string]: any;

  filter?: any;
  limit?: any;
  offset?: number;
  fields?: any[];
  populates?: any;
  transaction?: any;
  order?: any;
  cached?: boolean;
  allowNull?: boolean;
}
export interface ICrudExecOption {
  allowNull?: boolean;
}
