import express from 'express';
import { ErrorHelper } from '../helper/error/error.helper';
import { BaseController } from './baseController';
import { Request, Response } from './interfaces';

export class BaseRouter<T extends BaseController> {
  constructor(public path?: string) {}
  router: express.Router = express.Router();
  controller!: T;

  onError(res: Response, error: any) {
    if (!error.info) {
      console.log('UNKNOW ERROR', error);
      const err = ErrorHelper.somethingWentWrong();
      res.status(err.info.status).json(err.info);
    } else {
      res.status(error.info.status).json(error.info);
    }
  }

  route(func: (req: Request, rep: Response) => Promise<any>) {
    return (req: Request, res: Response) =>
      func
        .bind(this.controller)(req, res)
        .catch((error: any) => {
          this.onError(res, error);
        });
  }
}
