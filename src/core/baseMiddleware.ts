import { config } from '../config';
import { ErrorHelper } from '../helper/error/error.helper';
import { NextFunction, Request, Response } from './interfaces';

export class BaseMiddleware {
  onError(res: Response, error: any) {
    if (!error.info) {
      console.log('UNKNOW ERROR', error);
      if (config.debug) {
        const err = ErrorHelper.somethingWentWrong(error.message);
        res.status(err.info.status).json(err.info);
      } else {
        const err = ErrorHelper.somethingWentWrong();
        res.status(err.info.status).json(err.info);
      }
    } else {
      res.status(error.info.status).json(error.info);
    }
  }
  run(option?: any) {
    return (req: Request, res: Response, next: NextFunction) =>
      this.use
        .bind(this)(req, res, next, option)
        .catch((error: any) => {
          this.onError(res, error);
        });
  }
  async use(req: Request, res: Response, next: NextFunction, option?: any) {}
}
