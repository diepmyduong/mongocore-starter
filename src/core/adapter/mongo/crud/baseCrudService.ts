import { BaseDocument, Model, DocumentQuery } from '../baseModel';
import { BaseService } from '../../../baseService';
import { ICrudExecOption, ICrudOption } from '../../../interfaces';
import { config } from '../../../../config';
import { CacheHelper, Bucket } from '../../../../helper/cache/cache.helper';
import { ErrorHelper } from '../../../../helper/error/baseError';
import { UtilHelper } from '../../../../helper/util/util.helper';
export type CrudServiceOption = {
  cache?: {
    stdTTL?: number;
    checkperiod?: number;
    useClones?: boolean;
  };
};
export class BaseCrudService<T extends BaseDocument> extends BaseService {
  constructor(model: Model<T>, option: CrudServiceOption = {}) {
    super();
    this.model = model;
    this.bucket = CacheHelper.getBucket(
      Math.random()
        .toString()
        .substr(3, 15),
      option.cache
    );
  }
  model: Model<T>;
  bucket: Bucket;

  async exec<Y>(
    promise: any,
    option: ICrudExecOption = { allowNull: true }
  ): Promise<Y> {
    try {
      const result = await promise;
      if ((result === undefined || result === null) && !option.allowNull)
        throw ErrorHelper.mgRecoredNotFound();
      return result;
    } catch (err) {
      if (err.info) throw err;
      if (config.debug) {
        console.log('err', err);
        if (err.errors && err.errors[0]) {
          throw ErrorHelper.mgQueryFailed(err.errors[0].message);
        } else {
          throw ErrorHelper.mgQueryFailed(err.message);
        }
      } else throw err;
    }
  }
  async count(option: ICrudOption = {}): Promise<number> {
    let query = this.model.countDocuments({});
    query = this.applyQueryOptions(query, option);
    return await this.exec<number>(query);
  }
  async getList(option: ICrudOption = {}) {
    let hash = '';
    if (option.cached) {
      hash = `list-${UtilHelper.hash(option)}`;
      const result = CacheHelper.get<{ count: number; rows: T[] }>(
        hash,
        this.bucket.bucketName
      );
      if (result) return result;
    }
    let query = this.model.find();
    query = this.applyQueryOptions(query, option);
    query.setOptions({
      toJson: { virtual: true }
    });
    if (option.limit && option.limit > 0) query.limit(option.limit);
    if (option.offset) query.skip(option.offset);
    const [rows, count] = await Promise.all([
      this.exec<T[]>(query),
      this.count(option)
    ]);
    const result = { count, rows };
    if (option.cached) CacheHelper.set(hash, result, this.bucket.bucketName);
    return result;
  }
  async getItem(option: ICrudOption = {}) {
    let hash = '';
    if (option.cached) {
      hash = `item-${UtilHelper.hash(option)}`;
      const result = CacheHelper.get<T>(hash, this.bucket.bucketName);
      if (result) return result;
    }
    let query = this.model.findOne();
    query = this.applyQueryOptions(query, option);
    if (option.offset) query.skip(option.offset);
    const result = await this.exec<T>(query, { allowNull: option.allowNull });
    if (option.cached) CacheHelper.set(hash, result, this.bucket.bucketName);
    return result;
  }
  async create(params: T, option: ICrudOption = {}) {
    const query = this.model.create(params);
    this.bucket.cache.flushAll();
    return await this.exec<T>(query);
  }
  async update(params: T, option: ICrudOption = {}) {
    const query = this.model.findOneAndUpdate(option.filter, params, {
      new: true
    });
    this.bucket.cache.flushAll();
    return await this.exec<T>(query);
  }
  async delete(option: ICrudOption = {}) {
    let query = this.model.findOne();
    query = this.applyQueryOptions(query, option);
    const item = await this.exec<T>(query, { allowNull: false });
    this.bucket.cache.flushAll();
    return this.exec<T>(item.remove());
  }
  async deleteAll(option: ICrudOption = {}) {
    let query = this.model.remove(option.filter);
    query = this.applyQueryOptions(query, option);
    this.bucket.cache.flushAll();
    return await this.exec<T[]>(query);
  }
  applyQueryOptions(query: DocumentQuery, option: ICrudOption) {
    if (option.filter) query.where(option.filter);
    if (option.fields) query.select(option.fields);
    if (option.populates) {
      for (const populate of option.populates) {
        query.populate(populate);
      }
    }
    if (option.lean) query.lean();
    if (option.order) query.sort(option.order);
    return query;
  }
}
