import { BaseRouter } from '../../../baseRouter';
import { authMid } from '../../../../middlewares/auth.middleware';
import { queryMid } from '../../../../middlewares/query.middleware';
import { BaseCrudController } from './baseCrudController';

export class BaseCrudRouter<
  T extends BaseCrudController<any>
> extends BaseRouter<T> {
  constructor(public controller: T, public path?: string) {
    super(path);
    this.controller = controller;
    this.customRouting();
    this.defaultRouting();
  }
  defaultRouting() {
    this.router.get(
      '/count',
      this.countMiddlewares(),
      this.route(this.controller.count)
    );
    this.router.get(
      '/',
      this.getListMiddlewares(),
      this.route(this.controller.getList)
    );
    this.router.get(
      '/:_id',
      this.getItemMiddlewares(),
      this.route(this.controller.getItem)
    );
    this.router.post(
      '/',
      this.createMiddlewares(),
      this.route(this.controller.create)
    );
    this.router.put(
      '/:_id',
      this.updateMiddlewares(),
      this.route(this.controller.update)
    );
    this.router.delete(
      '/:_id',
      this.deleteMiddlewares(),
      this.route(this.controller.delete)
    );
    this.router.delete(
      '/',
      this.deleteAllMiddlewares(),
      this.route(this.controller.deleteAll)
    );
  }
  customRouting() {}
  countMiddlewares(): any[] {
    return [authMid.run(['admin', 'user.*']), queryMid.run()];
  }
  getListMiddlewares(): any[] {
    return [authMid.run(['admin', 'user.*']), queryMid.run()];
  }
  getItemMiddlewares(): any[] {
    return [authMid.run(['admin', 'user.*']), queryMid.run()];
  }
  createMiddlewares(): any[] {
    return [authMid.run(['admin', 'user.write'])];
  }
  updateMiddlewares(): any[] {
    return [authMid.run(['admin', 'user.write'])];
  }
  deleteMiddlewares(): any[] {
    return [authMid.run(['admin', 'user.write'])];
  }
  deleteAllMiddlewares(): any[] {
    return [authMid.run(['admin'])];
  }
}
