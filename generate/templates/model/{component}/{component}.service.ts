import { BaseCrudService } from '../../core/adapter/mongo/crud/baseCrudService';
import { {Name}Doc, {Name}Model } from './{name}.model';

export class {Name}Service extends BaseCrudService<{Name}Doc> {
  constructor() {
    super({Name}Model);
    console.log('contructor {Name}Service');
  }
}

export const {name}Service = new {Name}Service();
