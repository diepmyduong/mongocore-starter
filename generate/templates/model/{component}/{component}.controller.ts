import { BaseCrudController } from '../../core/adapter/mongo/crud/baseCrudController';
import { {Name}Schema } from './{name}.schema';
import { {Name}Service, {name}Service } from './{name}.service';

export class {Name}Controller extends BaseCrudController<{Name}Service> {
  constructor() {
    super({name}Service);
    this.createSchema = {Name}Schema.create;
  }
}

export const {name}Controller = new {Name}Controller();
