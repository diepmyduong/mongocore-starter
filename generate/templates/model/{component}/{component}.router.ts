import { BaseCrudRouter } from '../../core/adapter/mongo/crud/baseCrudRouter';
import { {name}Controller, {Name}Controller } from './{name}.controller';

export default class {Name}Router extends BaseCrudRouter<{Name}Controller> {
  constructor() {
    super({name}Controller, '{name}');
  }
  customRouting() {}
}
