import { BaseRouter } from '../../core/baseRouter';
import { {name}Controller, {Name}Controller } from './{name}.controller';

export default class {Name}Router extends BaseRouter<{Name}Controller> {
  constructor() {
    super('{name}');
    this.controller = {name}Controller;
    this.customRouting();
  }
  controller: {Name}Controller;

  customRouting() {
    this.router.post('/test', this.route(this.controller.test));
  }
}
