import { BaseController } from '../../core/baseController';
import { Request, Response } from '../../core/interfaces';
import { {Name}Schema } from './{name}.schema';

export class {Name}Controller extends BaseController {
  constructor() {
    super();
  }
  async test(req: Request, res: Response) {
    await this.validateJSON(req.body, {Name}Schema.test);
    this.onSuccess(res, { status: "success" });
  }
}

export const {name}Controller = new {Name}Controller();
