import { BaseMiddleware } from '../core/baseMiddleware';
import { NextFunction, Request, Response } from '../core/interfaces';

class {Name}InfoMiddleware extends BaseMiddleware {
  async use(req: Request, res: Response, next: NextFunction) {
    next();
  }
}

export const {name}Mid = new {Name}InfoMiddleware();
