"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const baseError_1 = require("./baseError");
class ErrorHelper extends baseError_1.ErrorHelper {
    static userNotExist() {
        return new baseError_1.BaseError(403, '-103', 'Người dùng khồng tồn tại');
    }
    static userExisted() {
        return new baseError_1.BaseError(403, '-104', 'Người dùng đã tồn tại');
    }
    static userRoleNotSupported() {
        return new baseError_1.BaseError(401, '-105', 'Người dùng không được cấp quyền');
    }
}
exports.ErrorHelper = ErrorHelper;
