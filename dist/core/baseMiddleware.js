"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const error_helper_1 = require("../helper/error/error.helper");
class BaseMiddleware {
    onError(res, error) {
        if (!error.info) {
            console.log('UNKNOW ERROR', error);
            if (config_1.config.debug) {
                const err = error_helper_1.ErrorHelper.somethingWentWrong(error.message);
                res.status(err.info.status).json(err.info);
            }
            else {
                const err = error_helper_1.ErrorHelper.somethingWentWrong();
                res.status(err.info.status).json(err.info);
            }
        }
        else {
            res.status(error.info.status).json(error.info);
        }
    }
    run(option) {
        return (req, res, next) => this.use
            .bind(this)(req, res, next, option)
            .catch((error) => {
            this.onError(res, error);
        });
    }
    use(req, res, next, option) {
        return __awaiter(this, void 0, void 0, function* () { });
    }
}
exports.BaseMiddleware = BaseMiddleware;
