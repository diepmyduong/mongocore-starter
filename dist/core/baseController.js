"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const error_helper_1 = require("../helper/error/error.helper");
const util_helper_1 = require("../helper/util/util.helper");
class BaseController {
    constructor() {
        this.pageSize = 25;
    }
    onSuccess(res, object = {}, extras = {}) {
        if (object === undefined)
            object = {};
        res.json({
            code: 200,
            data: lodash_1.default.merge(object, extras)
        });
    }
    onSuccessAsList(res, objects = [], total, option) {
        option = option || {};
        if (objects.toJSON) {
            objects = objects.toJSON();
        }
        const page = lodash_1.default.floor((option.offset || 0) / (option.limit || this.pageSize)) + 1;
        res.json({
            code: 200,
            data: objects,
            pagination: {
                currentPage: page,
                nextPage: page + 1,
                prevPage: page - 1,
                limit: option.limit,
                totalItems: total || 0
            }
        });
    }
    validateJSON(body, schema) {
        return __awaiter(this, void 0, void 0, function* () {
            const validate = util_helper_1.UtilHelper.validateJSON(schema, body);
            if (!validate.isValid) {
                throw error_helper_1.ErrorHelper.requestDataInvalid(validate.message);
            }
        });
    }
}
exports.BaseController = BaseController;
