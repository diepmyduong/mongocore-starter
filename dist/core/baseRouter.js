"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const error_helper_1 = require("../helper/error/error.helper");
class BaseRouter {
    constructor(path) {
        this.path = path;
        this.router = express_1.default.Router();
    }
    onError(res, error) {
        if (!error.info) {
            console.log('UNKNOW ERROR', error);
            const err = error_helper_1.ErrorHelper.somethingWentWrong();
            res.status(err.info.status).json(err.info);
        }
        else {
            res.status(error.info.status).json(error.info);
        }
    }
    route(func) {
        return (req, res) => func
            .bind(this.controller)(req, res)
            .catch((error) => {
            this.onError(res, error);
        });
    }
}
exports.BaseRouter = BaseRouter;
