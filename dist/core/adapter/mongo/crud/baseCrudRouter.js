"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const baseRouter_1 = require("../../../baseRouter");
const auth_middleware_1 = require("../../../../middlewares/auth.middleware");
const query_middleware_1 = require("../../../../middlewares/query.middleware");
class BaseCrudRouter extends baseRouter_1.BaseRouter {
    constructor(controller, path) {
        super(path);
        this.controller = controller;
        this.path = path;
        this.controller = controller;
        this.customRouting();
        this.defaultRouting();
    }
    defaultRouting() {
        this.router.get('/count', this.countMiddlewares(), this.route(this.controller.count));
        this.router.get('/', this.getListMiddlewares(), this.route(this.controller.getList));
        this.router.get('/:_id', this.getItemMiddlewares(), this.route(this.controller.getItem));
        this.router.post('/', this.createMiddlewares(), this.route(this.controller.create));
        this.router.put('/:_id', this.updateMiddlewares(), this.route(this.controller.update));
        this.router.delete('/:_id', this.deleteMiddlewares(), this.route(this.controller.delete));
        this.router.delete('/', this.deleteAllMiddlewares(), this.route(this.controller.deleteAll));
    }
    customRouting() { }
    countMiddlewares() {
        return [auth_middleware_1.authMid.run(['admin', 'user.*']), query_middleware_1.queryMid.run()];
    }
    getListMiddlewares() {
        return [auth_middleware_1.authMid.run(['admin', 'user.*']), query_middleware_1.queryMid.run()];
    }
    getItemMiddlewares() {
        return [auth_middleware_1.authMid.run(['admin', 'user.*']), query_middleware_1.queryMid.run()];
    }
    createMiddlewares() {
        return [auth_middleware_1.authMid.run(['admin', 'user.write'])];
    }
    updateMiddlewares() {
        return [auth_middleware_1.authMid.run(['admin', 'user.write'])];
    }
    deleteMiddlewares() {
        return [auth_middleware_1.authMid.run(['admin', 'user.write'])];
    }
    deleteAllMiddlewares() {
        return [auth_middleware_1.authMid.run(['admin'])];
    }
}
exports.BaseCrudRouter = BaseCrudRouter;
