"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseController_1 = require("../../../baseController");
const lodash_1 = __importDefault(require("lodash"));
class BaseCrudController extends baseController_1.BaseController {
    constructor(service) {
        super();
        this.createSchema = { type: 'object' };
        this.updateSchema = { type: 'object' };
        this.service = service;
    }
    dependFilter(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return {};
        });
    }
    appendCreateData(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return {};
        });
    }
    appendUpdateData(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return {};
        });
    }
    count(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = (yield this.service.count(req.queryInfo)) || 0;
            this.onSuccess(res, result);
        });
    }
    getList(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            lodash_1.default.set(req.queryInfo, 'filter', lodash_1.default.merge(lodash_1.default.get(req.queryInfo, 'filter'), yield this.dependFilter(req)));
            const { count, rows } = yield this.service.getList(req.queryInfo);
            this.onSuccessAsList(res, rows, count, req.queryInfo);
        });
    }
    getItem(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            lodash_1.default.set(req.queryInfo, 'filter', lodash_1.default.merge(lodash_1.default.get(req.queryInfo, 'filter'), yield this.dependFilter(req), {
                _id: req.params['_id']
            }));
            const result = yield this.service.getItem(req.queryInfo);
            this.onSuccess(res, result);
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.validateJSON(req.body, this.createSchema);
            const result = yield this.service.create(lodash_1.default.merge(req.body, yield this.appendCreateData(req)));
            this.onSuccess(res, result);
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.validateJSON(req.body, this.updateSchema);
            const result = yield this.service.update(lodash_1.default.merge(req.body, yield this.appendUpdateData(req)), {
                filter: lodash_1.default.merge({ _id: req.params._id }, yield this.dependFilter(req))
            });
            this.onSuccess(res, result);
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { _id } = req.params;
            const result = yield this.service.delete({
                filter: lodash_1.default.merge({ _id }, yield this.dependFilter(req))
            });
            this.onSuccess(res, result);
        });
    }
    deleteAll(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            if (lodash_1.default.has(req.query, 'items')) {
                req.query.items = JSON.parse(req.query.items) || [];
            }
            yield this.validateJSON(req.query, {
                type: 'object',
                properties: {
                    items: {
                        type: 'array',
                        uniqueItems: true,
                        minItems: 1,
                        items: { type: 'string' }
                    }
                },
                required: ['items'],
                additionalProperties: false
            });
            const { items } = req.query;
            const result = yield this.service.deleteAll({
                filter: lodash_1.default.merge({ _id: { $in: items } }, yield this.dependFilter(req))
            });
            this.onSuccess(res, result);
        });
    }
}
exports.BaseCrudController = BaseCrudController;
