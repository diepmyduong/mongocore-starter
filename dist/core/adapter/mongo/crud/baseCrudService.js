"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseService_1 = require("../../../baseService");
const config_1 = require("../../../../config");
const cache_helper_1 = require("../../../../helper/cache/cache.helper");
const baseError_1 = require("../../../../helper/error/baseError");
const util_helper_1 = require("../../../../helper/util/util.helper");
class BaseCrudService extends baseService_1.BaseService {
    constructor(model, option = {}) {
        super();
        this.model = model;
        this.bucket = cache_helper_1.CacheHelper.getBucket(Math.random()
            .toString()
            .substr(3, 15), option.cache);
    }
    exec(promise, option = { allowNull: true }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield promise;
                if ((result === undefined || result === null) && !option.allowNull)
                    throw baseError_1.ErrorHelper.mgRecoredNotFound();
                return result;
            }
            catch (err) {
                if (err.info)
                    throw err;
                if (config_1.config.debug) {
                    console.log('err', err);
                    if (err.errors && err.errors[0]) {
                        throw baseError_1.ErrorHelper.mgQueryFailed(err.errors[0].message);
                    }
                    else {
                        throw baseError_1.ErrorHelper.mgQueryFailed(err.message);
                    }
                }
                else
                    throw err;
            }
        });
    }
    count(option = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = this.model.countDocuments({});
            query = this.applyQueryOptions(query, option);
            return yield this.exec(query);
        });
    }
    getList(option = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            let hash = '';
            if (option.cached) {
                hash = `list-${util_helper_1.UtilHelper.hash(option)}`;
                const result = cache_helper_1.CacheHelper.get(hash, this.bucket.bucketName);
                if (result)
                    return result;
            }
            let query = this.model.find();
            query = this.applyQueryOptions(query, option);
            query.setOptions({
                toJson: { virtual: true }
            });
            if (option.limit && option.limit > 0)
                query.limit(option.limit);
            if (option.offset)
                query.skip(option.offset);
            const [rows, count] = yield Promise.all([
                this.exec(query),
                this.count(option)
            ]);
            const result = { count, rows };
            if (option.cached)
                cache_helper_1.CacheHelper.set(hash, result, this.bucket.bucketName);
            return result;
        });
    }
    getItem(option = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            let hash = '';
            if (option.cached) {
                hash = `item-${util_helper_1.UtilHelper.hash(option)}`;
                const result = cache_helper_1.CacheHelper.get(hash, this.bucket.bucketName);
                if (result)
                    return result;
            }
            let query = this.model.findOne();
            query = this.applyQueryOptions(query, option);
            if (option.offset)
                query.skip(option.offset);
            const result = yield this.exec(query, { allowNull: option.allowNull });
            if (option.cached)
                cache_helper_1.CacheHelper.set(hash, result, this.bucket.bucketName);
            return result;
        });
    }
    create(params, option = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = this.model.create(params);
            this.bucket.cache.flushAll();
            return yield this.exec(query);
        });
    }
    update(params, option = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = this.model.findOneAndUpdate(option.filter, params, {
                new: true
            });
            this.bucket.cache.flushAll();
            return yield this.exec(query);
        });
    }
    delete(option = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = this.model.findOne();
            query = this.applyQueryOptions(query, option);
            const item = yield this.exec(query, { allowNull: false });
            this.bucket.cache.flushAll();
            return this.exec(item.remove());
        });
    }
    deleteAll(option = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = this.model.remove(option.filter);
            query = this.applyQueryOptions(query, option);
            this.bucket.cache.flushAll();
            return yield this.exec(query);
        });
    }
    applyQueryOptions(query, option) {
        if (option.filter)
            query.where(option.filter);
        if (option.fields)
            query.select(option.fields);
        if (option.populates) {
            for (const populate of option.populates) {
                query.populate(populate);
            }
        }
        if (option.lean)
            query.lean();
        if (option.order)
            query.sort(option.order);
        return query;
    }
}
exports.BaseCrudService = BaseCrudService;
