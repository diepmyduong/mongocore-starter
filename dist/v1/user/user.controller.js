"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseCrudController_1 = require("../../core/adapter/mongo/crud/baseCrudController");
const error_helper_1 = require("../../helper/error/error.helper");
const firebase_helper_1 = require("../../helper/firebase/firebase.helper");
const user_schema_1 = require("./user.schema");
const user_service_1 = require("./user.service");
class UserController extends baseCrudController_1.BaseCrudController {
    constructor() {
        super(user_service_1.userService);
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.validateJSON(req.body, user_schema_1.UserSchema.createUserSchema);
            const { email, password } = req.body;
            const firebaseUser = yield firebase_helper_1.FirebaseHelper.createUser(email, password).catch(err => {
                throw error_helper_1.ErrorHelper.userExisted();
            });
            const user = Object.assign({}, req.body, { uid: firebaseUser.uid });
            delete user.password;
            if (req.authInfo.user.role != 'admin') {
                user.role = 'client';
            }
            const result = yield this.service.create(user);
            this.onSuccess(res, result);
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { _id } = req.params;
            const result = yield this.service.delete({
                filter: { id: _id }
            });
            firebase_helper_1.FirebaseHelper.removeUser(result.get('uid')).catch(error_helper_1.ErrorHelper.logError(`Error when remove firebase User ${result.uid}`));
            this.onSuccess(res, result);
        });
    }
}
exports.UserController = UserController;
exports.userController = new UserController();
