"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const main_1 = require("../../config/connections/main");
const Schema = mongoose_1.default.Schema;
const userSchema = new Schema({
    uid: { type: String, required: true, unique: true },
    name: { type: String, required: true },
    email: { type: String },
    picture: { type: String },
    phone: { type: String },
    role: { type: String, enum: ['admin', 'user'] }
}, { timestamps: true });
exports.UserModel = main_1.MainConnection.model('User', userSchema);
