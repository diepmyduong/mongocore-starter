"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const baseCrudRouter_1 = require("../../core/adapter/mongo/crud/baseCrudRouter");
const user_controller_1 = require("./user.controller");
class UserRouter extends baseCrudRouter_1.BaseCrudRouter {
    constructor() {
        super(user_controller_1.userController);
    }
    customRouting() { }
}
exports.default = UserRouter;
