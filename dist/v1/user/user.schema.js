"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserSchema {
}
UserSchema.createUserSchema = {
    type: 'object',
    properties: {
        name: { type: 'string' },
        email: { type: 'string' },
        phone: { type: 'string' },
        address: { type: 'string' },
        password: { type: 'string' },
        role: { type: 'string' }
    },
    required: ['name', 'email', 'password'],
    additionalProperties: false
};
exports.UserSchema = UserSchema;
