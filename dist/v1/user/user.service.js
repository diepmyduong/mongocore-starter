"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const baseCrudService_1 = require("../../core/adapter/mongo/crud/baseCrudService");
const user_model_1 = require("./user.model");
class UserService extends baseCrudService_1.BaseCrudService {
    constructor() {
        super(user_model_1.UserModel);
        console.log('contructor UserService');
    }
}
exports.UserService = UserService;
exports.userService = new UserService();
