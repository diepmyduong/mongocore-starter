"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const express_1 = __importDefault(require("express"));
function regisRouter(route, node, dir) {
    const modules = fs_1.default
        .readdirSync(dir)
        .filter(m => fs_1.default.lstatSync(path_1.default.join(dir, m)).isDirectory());
    modules.forEach(modulePath => {
        const routerPath = fs_1.default
            .readdirSync(path_1.default.join(dir, modulePath))
            .find(f => /(.*).router.js$/.test(f));
        if (routerPath) {
            const { default: Router } = require(path_1.default.join(dir, modulePath, routerPath));
            const router = new Router();
            route.use(`/${router.path || /(.*).router.js$/.exec(routerPath)[1]}`, router.router);
        }
    });
    return route;
}
exports.default = regisRouter(express_1.default.Router(), '', __dirname);
