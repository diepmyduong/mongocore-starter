"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const baseRouter_1 = require("../../core/baseRouter");
const auth_controller_1 = require("./auth.controller");
class AuthRouter extends baseRouter_1.BaseRouter {
    constructor() {
        super('auth');
        this.controller = auth_controller_1.authController;
        this.customRouting();
    }
    customRouting() {
        this.router.post('/login', this.route(this.controller.login));
    }
}
exports.default = AuthRouter;
