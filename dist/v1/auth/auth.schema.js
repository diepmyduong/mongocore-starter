"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AuthSchema {
}
AuthSchema.LoginSchema = {
    type: 'object',
    properties: {
        token: { type: 'string' },
        name: { type: 'string' },
        phone: { type: 'string' },
        email: { type: 'string' },
        address: { type: 'string' }
    },
    required: ['token', 'name', 'email', 'phone'],
    additionalProperties: false
};
exports.AuthSchema = AuthSchema;
