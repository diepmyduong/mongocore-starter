"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const token_helper_1 = require("../../helper/token/token.helper");
class AuthHelper {
    static getUserToken(user) {
        const json = user.toJSON();
        const result = {
            token: token_helper_1.TokenHelper.generateToken(lodash_1.default.omit(json), user.role)
        };
        return result;
    }
}
exports.AuthHelper = AuthHelper;
