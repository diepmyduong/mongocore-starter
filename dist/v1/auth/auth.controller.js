"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseController_1 = require("../../core/baseController");
const cache_helper_1 = require("../../helper/cache/cache.helper");
const error_helper_1 = require("../../helper/error/error.helper");
const firebase_helper_1 = require("../../helper/firebase/firebase.helper");
const user_service_1 = require("../user/user.service");
const auth_helper_1 = require("./auth.helper");
const auth_schema_1 = require("./auth.schema");
class AuthController extends baseController_1.BaseController {
    constructor() {
        super();
    }
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.validateJSON(req.body, auth_schema_1.AuthSchema.LoginSchema);
            const { token, name, email, phone, address } = req.body;
            const [type, uid, fbtoken] = token.split('|');
            if (type == 'FB') {
                const decode = yield firebase_helper_1.FirebaseHelper.verifyIdToken(fbtoken);
                if (uid != decode.uid)
                    throw error_helper_1.ErrorHelper.badToken();
                let user = yield user_service_1.userService.getItem({
                    filter: { uid: decode.uid },
                    allowNull: true,
                    cached: true
                });
                if (!user) {
                    let userCount = cache_helper_1.CacheHelper.get('user-count');
                    if (!userCount) {
                        userCount = yield user_service_1.userService.count();
                        if (userCount > 0)
                            cache_helper_1.CacheHelper.set('user-count', userCount);
                    }
                    user = new user_service_1.userService.model({
                        uid: decode.uid,
                        email: email || decode.email,
                        name: name || decode.email,
                        phone: phone,
                        address: address
                    });
                    if (userCount == 0) {
                        user.role = 'admin';
                    }
                    yield user.save();
                }
                this.onSuccess(res, auth_helper_1.AuthHelper.getUserToken(user));
            }
            else {
                throw error_helper_1.ErrorHelper.badToken();
            }
        });
    }
}
exports.AuthController = AuthController;
exports.authController = new AuthController();
