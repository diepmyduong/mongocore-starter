"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseError_1 = require("../helper/error/baseError");
const token_helper_1 = require("../helper/token/token.helper");
const user_service_1 = require("../v1/user/user.service");
const baseMiddleware_1 = require("../core/baseMiddleware");
class AuthInfoMiddleware extends baseMiddleware_1.BaseMiddleware {
    use(req, res, next, providers = []) {
        return __awaiter(this, void 0, void 0, function* () {
            if (providers.indexOf('block') != -1) {
                throw baseError_1.ErrorHelper.permissionDeny();
            }
            if (!req.headers['x-token'] && req.query['x-token']) {
                req.headers['x-token'] = req.query['x-token'];
            }
            if (req.headers['x-token']) {
                const [type, id, token] = req.headers['x-token'].toString().split('|');
                req.authInfo = { id, provider: '*' };
                switch (type) {
                    case 'U':
                        const decodeToken = token_helper_1.TokenHelper.decodeToken(token);
                        req.authInfo.user = new user_service_1.userService.model(decodeToken.payload);
                        req.authInfo.provider = decodeToken.role;
                        yield this.permissionVerify(req, providers);
                        break;
                    case 'ANY':
                        if (!providers.includes('*')) {
                            throw baseError_1.ErrorHelper.permissionDeny();
                        }
                        break;
                    default:
                        throw baseError_1.ErrorHelper.badToken();
                }
                next();
                return;
            }
            throw baseError_1.ErrorHelper.unauthorized();
        });
    }
    permissionVerify(req, providers, option = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            for (const p of providers) {
                const regex = new RegExp('^' + p + '$');
                for (const r of req.authInfo.provider.split(' ')) {
                    if (regex.test(r))
                        return true;
                }
            }
            throw baseError_1.ErrorHelper.permissionDeny();
        });
    }
}
exports.AuthInfoMiddleware = AuthInfoMiddleware;
exports.authMid = new AuthInfoMiddleware();
