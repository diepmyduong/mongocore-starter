"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const config_1 = require("../config");
const baseMiddleware_1 = require("../core/baseMiddleware");
class QueryMiddleware extends baseMiddleware_1.BaseMiddleware {
    use(req, res, next, option) {
        return __awaiter(this, void 0, void 0, function* () {
            const filter = this._parseFilter(req, option);
            const order = this._parseOrder(req);
            const page = parseInt(req.query['page'] || 1);
            const limit = parseInt(req.query['limit'] || config_1.config.pageSize);
            const offset = parseInt(req.query['offset']) || (page - 1) * limit;
            const fields = this._parseMongoFields(req);
            const populates = this._parsePopulates(req);
            req.queryInfo = lodash_1.default.merge({
                filter,
                limit,
                offset,
                fields,
                populates,
                order,
                cached: req.query['cached'] == "true"
            });
            next();
        });
    }
    _parseFilter(req, option) {
        let filter = req.query['filter'];
        try {
            filter = JSON.parse(filter);
        }
        catch (ignore) {
            filter = undefined;
        }
        if (option && option.useBody) {
            filter = req.body['filter'];
        }
        return filter || {};
    }
    _parseGroup(req) {
        let group = req.query['group'];
        try {
            group = JSON.parse(group);
        }
        catch (err) {
            group = undefined;
        }
        return group;
    }
    _parseOrder(req) {
        let order = req.query['order'];
        try {
            order = JSON.parse(order);
        }
        catch (ignore) {
            order = undefined;
        }
        return order || [];
    }
    _parseMongoFields(req) {
        let fields = req.query['fields'];
        try {
            fields = JSON.parse(fields);
        }
        catch (ignore) {
            fields = undefined;
        }
        return fields;
    }
    _parsePopulates(req) {
        let populates = req.query['populates'];
        try {
            populates = JSON.parse(populates);
        }
        catch (ignore) {
            populates = undefined;
        }
        return populates;
    }
}
exports.queryMid = new QueryMiddleware();
