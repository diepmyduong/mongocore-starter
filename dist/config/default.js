"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DefaultConfig {
    constructor() {
        this.name = 'Mongo Node Core';
        this.protocol = 'http';
        this.host = 'localhost';
        this.port = process.env['PORT'] || '5050';
        this.secret = process.env['SECRET'] || 'hellokitty';
        this.domain = 'http://localhost:5050';
        this.timezone = 'Asia/Ho_Chi_Minh';
        this.debug = true;
        this.pageSize = 20;
        this.mgConnection = process.env['MG_CONNECTION'] ||
            'mongodb+srv://user:pass@localhost/maindb?retryWrites=true';
        this.firebaseCredential = {
            private_key: process.env.FIREBASE_PRIVATE_KEY
                ? process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n')
                : '',
            type: 'service_account',
            project_id: 'postol',
            private_key_id: '77d570eebe474e4569ce0ce1c16443defd8cabb7',
            client_email: 'firebase-adminsdk-itctc@postol.iam.gserviceaccount.com',
            client_id: '109270553907865385270',
            auth_uri: 'https://accounts.google.com/o/oauth2/auth',
            token_uri: 'https://oauth2.googleapis.com/token',
            auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
            client_x509_cert_url: 'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-itctc%40postol.iam.gserviceaccount.com'
        };
        this.enableRealtimeConfig = false;
    }
}
exports.DefaultConfig = DefaultConfig;
