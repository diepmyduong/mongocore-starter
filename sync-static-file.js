var Rsync = require('rsync');
var shell = require('shelljs');
 
// Build the command
var rsync = Rsync.build({
    source:      ['./src/templates', './src/public'],
    destination: './dev',
    flags:       'avh',
    delete:      true
});
 
// Execute the command
rsync.execute(function(error, code, cmd) {
    console.log('cmd', cmd);
    if (error) {
        console.log("ERROR", error.message, code, cmd);
        shell.cp('-Rf', "src/templates", "dev/");
        shell.cp('-Rf', "src/public", "dev/");
    } else {
        console.log("SYNC STATIC FILE DONE");
    }
});